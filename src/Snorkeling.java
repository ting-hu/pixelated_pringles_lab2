
import java.util.ArrayList;

public class Snorkeling
{
    ArrayList<Snorkeling> SnorkelingList = new ArrayList<> ();

    public void run()
    {
        // Create two Octopuses
        Octopus Oct1 = new Octopus();
        Octopus Oct2 = new Octopus("Will","yellow", 3);

        // Create two Sea Turtles
        SeaTurtle SeaTur1 = new SeaTurtle();
        SeaTurtle SeaTur2 = new SeaTurtle("Crush","Dark Green", 7);

        // Create two Blue Tang
        BlueTang BlueTang1 = new BlueTang();
        BlueTang BlueTang2 = new BlueTang("Charlie", 4);

        // Display the attributes of all the sea pets
        System.out.println(Oct1.toString());
        System.out.println(Oct2.toString());
        System.out.println(SeaTur1.toString());
        System.out.println(SeaTur2.toString());
        System.out.println(BlueTang1.toString());
        System.out.println(BlueTang2.toString());

        
    }
}