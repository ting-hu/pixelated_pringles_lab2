public class SeaTurtle
{
    String name;
    String color;
    int age;

    SeaTurtle()
    {
        name = "Squirt";
        color = "Green";
        age = 1;
    }

    SeaTurtle(String name, String color, int age)
    {
        this.name = name;
        this.color = color;
        this.age = age;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getColor()
    {
        return color;
    }

    public void setColor(String color)
    {
        this.color = color;
    }

    public int getAge()
    {
        return age;
    }

    public void setAge(int age)
    {
        this.age = age;
    }

    @Override
    public String toString()
    {
        return this.name + " is a " + this.color + " Sea Turtle that is " + this.age + " years old.";
    }
}