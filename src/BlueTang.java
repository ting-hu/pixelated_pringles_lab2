public class BlueTang
{
    String name;
    int age;

    BlueTang()
    {
        name = "Dory";
        age = 1;
    }

    BlueTang(String name, int age)
    {
        this.name = name;
        this.age = age;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getAge()
    {
        return age;
    }

    public void setAge(int age)
    {
        this.age = age;
    }

    @Override
    public String toString()
    {
        return this.name + " is a Blue Tang fish that is " + this.age + " years old.";
    }
}
